<?php
/**
 * lemocms
 * ============================================================================
 * 版权所有 2018-2027 lemocms，并保留所有权利。
 * 网站地址: https://www.lemocms.com
 * ----------------------------------------------------------------------------
 * 采用最新Thinkphp6实现
 * ============================================================================
 * Author: yuege
 * Date: 2019/8/2
 */

namespace app\admin\controller;

use app\admin\model\Admin;
use app\admin\model\AuthRule;
use app\common\controller\Backend;
use http\Cookie;
use lemo\helper\SignHelper;
use think\facade\Request;
use think\facade\View;
use think\facade\Db;
use think\facade\Cache;
use think\facade\Session;
use app\common\lib\Menu;

class Index extends Backend
{

    public function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated stub

    }

    /**
     * @return string
     * @throws \Exception
     * 首页
     */
    public function index()
    {
        // 所有显示的菜单；
        $admin_id = Session::get('admin.id');
//        $menus = Cache::get('adminMenus_'.$admin_id);
//        if(!$menus){
//            $cate = AuthRule::where('menu_status',1)->order('sort asc')->select()->toArray();
//            $menus = Menu::authMenu($cate);
//            cache('adminMenus_'.$admin_id,$menus,['expire'=>3600]);
//
//        }
//        $href = (string)url('main');
//        $home = ["href"=>$href,"icon"=>"fa fa-home","title"=>"首页"];
//        $menusinit =['menus'=>$menus,'home'=>$home];
//        View::assign('menus',json_encode($menusinit));
        return view();
    }

    public function menus()
    {
        $admin_id = Session::get('admin.id');
        $menus = json_decode(cookie('adminMenus_' . $admin_id));
        if (!$menus) {
            $cate = AuthRule::where('menu_status', 1)->order('sort asc')->select()->toArray();
            $menus = Menu::authMenu($cate);
//            cookie('adminMenus_'.$admin_id,json_encode($menus),['expire'=>3600]);
        }
        $href = (string)url('main');
        $home = ["href" => $href, "icon" => "fa fa-home", "title" => "首页"];
//        $logoInfo = ["title" => "LEMOCMS", "image" => "/static/admin/images/logo.png", "href" => "https://www.lemocms.com"];
        $logoInfo = ["title" => "学生评价系统", "href" => "https://www.lemocms.com"];
        $menusinit = ['menuInfo' => $menus, 'homeInfo' => $home, 'logoInfo' => $logoInfo];
        return json($menusinit);

    }

    /**
     * @return string
     * @throws \think\db\exception\BindParamException
     * @throws \think\db\exception\PDOException
     * 主页面
     */
    public function main()
    {
//        $version = Db::query('SELECT VERSION() AS ver');
//        $version = [0=>1];
        $config = Cache::get('main_config');
        if (!$config) {

            $config = [
                'url' => $_SERVER['HTTP_HOST'],
                'document_root' => $_SERVER['DOCUMENT_ROOT'], //系统函数查询配置
                'php_version' => PHP_VERSION,
            ];
            Cache::set('main_config', $config, 3600);
        }

        View::assign('config', $config);

        return view();
    }

}